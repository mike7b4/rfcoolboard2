Second generaration RF controler.

PCB supports:

* STM32F042/STM32F078
* 2 dual mosfets connected to STM. Gives up to 4 PWM outputs.
* Switchregulator MCP16311 support voltage between 6 and 15v OUT set to 5v.
* LDO MCP1702 5v => 3.3v or use 2.2v LDO if using coin cell.
* Coin cell battery connected to LDO. (DO NOT MOUNT MCP16311 IF USING COIN CELL!!!!)
* "Standard" USB addon pin out connector.
* "Standard" NRF pinout connector for most NRF24LO+ modules.
* One PCB mounted LED connected to STM PA6.
* 7 in/out pin connectors for custom stuff.
* SWD pinout.
* One INPUT terminal block.
* 4 OUT terminal connected via FET's and input.

